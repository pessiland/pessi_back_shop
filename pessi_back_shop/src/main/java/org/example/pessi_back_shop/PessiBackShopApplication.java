package org.example.pessi_back_shop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class PessiBackShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(PessiBackShopApplication.class, args);
	}

}
