package org.example.pessi_back_shop;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.pessi_back_shop.model.ShopItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@EnableScheduling
@Service
public class ShopService {

    private final RedisTemplate<String, String> redisProduct;
    private final RedisTemplate<String, Set<String>> redisPurchase;
    private final RestTemplate restTemplate;
    private final String inventoryServiceLocation = "http://localhost:8084/inventory";

    @Autowired
    public ShopService(RedisTemplate<String, String> redisProduct, RedisTemplate<String, Set<String>> redisPurchase, RestTemplate restTemplate) {
        this.redisProduct = redisProduct;
        this.redisPurchase = redisPurchase;
        this.restTemplate = restTemplate;
    }

    public List<ShopItem> getAvailableItems(int playerId) {
        List<String> itemIds = List.of("item0", "item1", "item2", "item3", "item4", "item5", "item6", "item7", "item8", "item9");
        List<ShopItem> availableItems = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();

        List<String> availableItemsJson = redisProduct.opsForValue().multiGet(itemIds);
        Set<String> purchasedItemIds = redisPurchase.opsForValue().get(String.valueOf(playerId));

        if (availableItemsJson != null) {
            for (String itemJson : availableItemsJson) {
                try {
                    ShopItem item = objectMapper.readValue(itemJson, ShopItem.class);
                    if (purchasedItemIds != null && purchasedItemIds.contains(String.valueOf(item.getId()))) {
                        item.setPurchased(true);
                    }
                    availableItems.add(item);
                } catch (JsonProcessingException e) {
                    System.out.println(e.getMessage());
                }
            }
        }

        return availableItems;
    }

    public boolean purchaseItem(String item, int playerId) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            ShopItem itemToBuy = objectMapper.readValue(item, ShopItem.class);

            Set<String> purchasedItemIds = redisPurchase.opsForValue().get(String.valueOf(playerId));
            if (purchasedItemIds != null && purchasedItemIds.contains(String.valueOf(itemToBuy.getId()))) {
                return false;
            }

            String purchasedItemJson = redisProduct.opsForValue().get("item" + itemToBuy.getId());

            if (purchasedItemJson != null) {


                ShopItem purchasedItem = objectMapper.readValue(purchasedItemJson, ShopItem.class);
                if (purchasedItem.getPrice() == itemToBuy.getPrice() && purchasedItem.getType().equals(itemToBuy.getType())) {

                    String updateBalanceUrl = inventoryServiceLocation + "/balance/update";

                    HttpHeaders headers = new HttpHeaders();
                    headers.set("playerId", String.valueOf(playerId));

                    HttpEntity<Integer> request = new HttpEntity<>(-purchasedItem.getPrice(), headers);

                    boolean hasEnoughMoney = restTemplate.postForObject(updateBalanceUrl, request, Boolean.class);

                    if (hasEnoughMoney) {
                        String updateQuantityUrl = inventoryServiceLocation + "/add";

                        if (purchasedItem.getType().equals("Egg")) {
                            HttpEntity<String> requestEgg = new HttpEntity<>("Egg", headers);
                            restTemplate.postForObject(updateQuantityUrl, requestEgg, Void.class);
                        } else {
                            HttpEntity<String> requestIncubator = new HttpEntity<>("Incubator", headers);
                            restTemplate.postForObject(updateQuantityUrl, requestIncubator, Void.class);
                        }

                        if (purchasedItemIds == null) {
                            purchasedItemIds = new HashSet<>();
                        }

                        purchasedItemIds.add(String.valueOf(itemToBuy.getId()));

                        redisPurchase.opsForValue().set(String.valueOf(playerId), purchasedItemIds);

                        return true;
                    }
                }
            }
        } catch (JsonProcessingException e) {
            System.out.println(e.getMessage());
        }

        return false;
    }

    public boolean sellItem(String item, int playerId) {

        if (item.equals("Egg")) {
            String updateEggQuantity = inventoryServiceLocation + "/egg/remove";

            HttpHeaders headers = new HttpHeaders();
            headers.set("playerId", String.valueOf(playerId));

            HttpEntity<String> requestRemoveEgg = new HttpEntity<>(null, headers);

            boolean eggRemoved = restTemplate.postForObject(updateEggQuantity, requestRemoveEgg, Boolean.class);
            if (eggRemoved) {

                String updateBalanceUrl = inventoryServiceLocation + "/balance/update";

                HttpEntity<Integer> requestUpdateBalance = new HttpEntity<>(5, headers);

                restTemplate.postForObject(updateBalanceUrl, requestUpdateBalance, Void.class);

                return true;
            }
        }
        return false;

    }


    @Scheduled(fixedRate = 600000)
    private void refreshItems() {
        try {
            generateAndStoreRandomItems(10);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void generateAndStoreRandomItems(int count) {
        Set<String> productKeys = redisProduct.keys("*");
        redisProduct.delete(productKeys);

        Set<String> purchaseKeys = redisPurchase.keys("*");
        redisPurchase.delete(purchaseKeys);

        Random random = new Random();

        ObjectMapper objectMapper = new ObjectMapper();

        for (int i = 0; i < count; i++) {
            ShopItem item = new ShopItem();
            item.setType(random.nextBoolean() ? "Egg" : "Incubator");
            item.setId(i);

            int price = random.nextInt(10) + 1;
            item.setPrice(price);
            item.setPurchased(false);

            try {
                String jsonItem = objectMapper.writeValueAsString(item);
                redisProduct.opsForValue().set("item" + i, jsonItem);
            } catch (JsonProcessingException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
