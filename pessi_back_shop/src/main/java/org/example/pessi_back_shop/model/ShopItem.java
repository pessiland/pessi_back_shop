package org.example.pessi_back_shop.model;

public class ShopItem {
    private int id;
    private String type;
    private int price;
    private boolean purchased;

    public void setId(int i) { this.id = i; }

    public void setType(String s) {
        this.type = s;
    }

    public void setPrice(int v) {
        this.price = v;
    }

    public void setPurchased(boolean b) { this.purchased = b; }

    public int getPrice() {
        return this.price;
    }

    public String getType() {
        return this.type;
    }

    public int getId() {
        return this.id;
    }

    public boolean getPurchased() { return this.purchased; }
}
