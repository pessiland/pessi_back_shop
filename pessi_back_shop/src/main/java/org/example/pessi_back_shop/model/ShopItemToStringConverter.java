package org.example.pessi_back_shop.model;

import org.springframework.core.convert.converter.Converter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class ShopItemToStringConverter implements Converter<ShopItem, String> {

    private final ObjectMapper objectMapper;

    public ShopItemToStringConverter(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public String convert(ShopItem shopItem) {
        try {
            return objectMapper.writeValueAsString(shopItem);
        } catch (Exception e) {
            throw new RuntimeException("Error converting ShopItem to String", e);
        }
    }
}