package org.example.pessi_back_shop;

import org.example.pessi_back_shop.model.ShopItem;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/shop")
public class ShopController {

    private final ShopService shopService;

    public ShopController(ShopService shopService) {
        this.shopService = shopService;
    }

    @GetMapping("/getAvailableItem")
    public List<ShopItem> getAvailableItems(@RequestHeader int playerId) {
        return shopService.getAvailableItems(playerId);
    }

    @PostMapping("/buy")
    public boolean purchaseItem(@RequestBody String item, @RequestHeader int playerId) {
        return shopService.purchaseItem(item, playerId);
    }

    @PostMapping("/sell")
    public boolean sellItem(@RequestBody String item, @RequestHeader int playerId) {
        return shopService.sellItem(item, playerId);
    }
}
