FROM eclipse-temurin:21-jdk-alpine
VOLUME /tmp
WORKDIR /app
COPY ./target/*.jar /app
EXPOSE 8080
ENTRYPOINT ["sh", "-c", "java -jar *.jar"]